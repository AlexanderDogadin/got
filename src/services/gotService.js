export default class GotService{
    constructor(){
        this._urlBase = "https://www.anapioficeandfire.com/api";
    }
    getResource = async (url) => {
        const fullUrl = this._urlBase + url;
        const res = await fetch(fullUrl);
        if(!res.ok){
            throw new Error(`Could not fetch ${fullUrl}, status: ${res.status}` )
        }
        return await res.json();
    }

    getAllCharacters = async (page = 20, pageSize = 10) => {
        const res = await this.getResource(`/characters?page=${page}&pageSize=${pageSize}`);
        return res.map(this._transformCharacter);
    }
    getCharacter = async (id) => {
        const res = await this.getResource(`/characters/${id}`)
        return this._transformCharacter(res);
    }
    getAllBooks = async (page = 1, pageSize = 1000) => {
        const res = await this.getResource(`/books?page=${page}&pageSize=${pageSize}`);
        return res.map(this._transformBook);
    }
    getBook = async (id) => {
        const res = await this.getResource(`/books/${id}`)
        return this._transformBook(res);
    }
    getAllHouses = async (page = 1, pageSize = 1000) => {
        const res = await this.getResource(`/houses?page=${page}&pageSize=${pageSize}`);
        return res.map(this._transformHouse);
    }
    getHouse = async (id) => {
        const res = await this.getResource(`/houses/${id}`)
        return this._transformHouse(res);
    }

    // setNoEmpty = (data) => {
    //     console.log("data:",data);
    //     return(
    //         Object.fromEntries(
    //             Object.entries(data).map(([key,value])=>
    //                 [key, value ? value : 'не определено' ]
    //             )
    //         )    
    //     )
    // }

    setNotNull(data){
        return data? data : "not defined";
    }

    getUnique(data){
        return data.match(/\d+$/).join() 
    }

    _transformCharacter = (char) => {

        // return {
        //     name: char.name,
        //     gender: char.gender,
        //     born: char.born,
        //     died: char.died,
        //     culture: char.culture
        // }

        // //убираем пустые значения
        // console.log("char:",char);

        // const ret = this.setNoEmpty(char);

        // console.log("ret:",ret);
        // console.log("id:",this._getUnique(ret.url));
        
        // return {
        //     id: this._getUnique(ret.url),
        //     name: ret.name,
        //     gender: ret.gender,
        //     born: ret.born,
        //     died: ret.died,
        //     culture: ret.culture
        // }

        const {url,name,gender,born,died,culture} = char;

        return {
            id: this.getUnique(url),
            name: this.setNotNull(name),
            gender: this.setNotNull(gender),
            born: this.setNotNull(born),
            died: this.setNotNull(died),
            culture: this.setNotNull(culture)
        }

    }

    _transformHouse = (house) =>{
        // const ret = this._setEmpty(house);
        // return {
        //     id: this._getUnique(ret.url),
        //     name: ret.name,
        //     region: ret.region,
        //     words: ret.words,
        //     titles: ret.titles,
        //     overlord: ret.overlord,
        //     ancestralWeapons: ret.ancestralWeapons
        // }
        const {url,name,region,words,titles,overlord,ancestralWeapons} = house;
        return {
            id: this.getUnique(url),
            name: this.setNotNull(name),
            region: this.setNotNull(region),
            words: this.setNotNull(words),
            titles: this.setNotNull(titles),
            overlord: this.setNotNull(overlord),
            ancestralWeapons: this.setNotNull(ancestralWeapons)
        }
    }

    _transformBook = (book) => {
        // const ret = this._setEmpty(book);
        // return{
        //     id: this._getUnique(ret.url),
        //     name: ret.name,
        //     numberOfPages: ret.numberOfPages,
        //     publiser: ret.publiser,
        //     released: ret.released
        // }
        const {url,name,numberOfPages,publiser,released} = book;
        return {
            id: this.getUnique(url),
            name: this.setNotNull(name),
            numberOfPages: this.setNotNull(numberOfPages),
            publiser: this.setNotNull(publiser),
            released: this.setNotNull(released)
        }
    }
}
