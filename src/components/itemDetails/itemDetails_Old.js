import React, {Component} from 'react';
import './itemDetails.css';
// import GotService from '../../services/gotService';
import Spinner from '../spiner';
import ErrorMessage from '../errorMessage'

// const Field = ({item,field,label})=>{
//     return (
//         <li className="list-group-item d-flex justify-content-between">
//             <span className="term">{label}</span>
//             <span>{item[field]}</span>
//         </li>
//     )
// }

const Field = ({item,field,label})=>{
    return (
        <li className="list-group-item d-flex justify-content-between">
            <span className="term">{label}</span>
            <span>{item[field]}</span>
        </li>
    )
}


export {
    Field
}

export default class ItemDetails extends Component {
    state = {
        item:null,
        onLoad: false,
        error: null,
    }

    // gotService = new GotService();

    updateItem () {
        const {itemId, getData} = this.props;
        if (!itemId){
            return;
        }

        this.setState({item:null, onLoad:true, error:null})

        getData(itemId)
            .then((item) =>{
                this.setState({item, onLoad:false, error: null})
            })
            .catch((error)=>this.onError(error))
        // this.foo.bar = 0
    }

    onError(err){
        this.setState({error:err})
    }

    componentDidMount(){
        this.updateItem();
    }

    componentDidUpdate(prevState){
        if(this.props.itemId !== prevState.itemId){
            this.updateItem()
        }
    }

    render() {
        const {item,onLoad,error} = this.state;
        // if (error) { return <ErrorMessage msg={error.message}/> }
        if (error) { return <ErrorMessage msg = {error.message}/> }
        if (!item && onLoad) { return <Spinner/> } 
        if (!item) { return <span className = 'select-error'>Select entry, please</span> } 

        const {name} = item;

        return (
            <div className="item-details rounded">
                <h4>{name}</h4>
                <ul className="list-group list-group-flush">
                    {/* {this.props.children} */}
                    {
                        React.Children.map(this.props.children, (chield)=>{
                            return React.cloneElement(chield,{item})
                        })
                    }
                </ul>
            </div>
        );
    }
}