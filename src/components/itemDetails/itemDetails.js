import React, {useState, useEffect} from 'react';
import './itemDetails.css';
import Spinner from '../spiner';
import ErrorMessage from '../errorMessage'

const Field = ({item,field,label})=>{
    return (
        <li className="list-group-item d-flex justify-content-between">
            <span className="term">{label}</span>
            <span>{item[field]}</span>
        </li>
    )
}

export {
    Field
}

function ItemDetails({getData, itemId, children}) {

    const [state,updateState] = useState({item:null,onLoad:false,error:null})

    function updateItem (item,onLoad,error) {
        updateState(prevState=>({
            ...prevState,
            item, 
            onLoad, 
            error
        }))
    }

    useEffect(()=>{
        if (!itemId){return}
        updateItem(null,true,null)
        getData(itemId)
        .then((data) => {
                updateItem(data,false,null)
            })
        .catch((err)=>{
            updateItem(null,false,err)
        })
    },[itemId])  

    const {item,onLoad,error} = state;
    if (error) { return <ErrorMessage msg = {error.message}/> }
    if (!item && onLoad) { return <Spinner/> } 
    if (!item) { return <span className = 'select-error'>Select entry, please</span> } 

    const {name} = item;

    return (
        <div className="item-details rounded">
            <h4>{name}</h4>
            <ul className="list-group list-group-flush">
                {
                    React.Children.map(children, (chield)=>{
                        return React.cloneElement(chield,{item})
                    })
                }
            </ul>
        </div>
    );
}

export default ItemDetails