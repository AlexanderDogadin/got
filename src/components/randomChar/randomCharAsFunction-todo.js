import React, {useState, useEffect} from 'react';
import './randomChar.css';
import GotService from '../../services/gotService';
import Spinner from '../spiner';
import ErrorMessage from '../errorMessage';

function RandomChar ({interval}){
    interval = 10000
    const [state, updateState] = useState({char:{},loading:true,error:null, id:100, timerId: null})

    const gotService = new GotService();

    function onCharLoaded (char) {
        updateState(prevState=>({
            ...prevState,
            char,
            loading:false,
            error:null
        }))
    }

    function onError (error) {
        updateState(prevState=>({
            ...prevState,
            loading:false,
            error
        }))
    }
    const timerID = setInterval(updateChar, interval);

    useEffect(()=>{
        gotService.getCharacter(state.id)
            .then(data=>onCharLoaded(data))
            .catch(error=>onError(error))
        return(()=>clearInterval(state.timerID))
    },[state.id])

    function updateChar () {
        const id = Math.floor(Math.random()*140+25);
        updateState(prevState=>({
            ...prevState,
            loading:true,
            id
        }))
    }

    const {char, loading, error} = state;

    const errorMessage = error ? <ErrorMessage msg = {error.message}/> : null
    const spinner = loading ? <Spinner/> : null;
    const content = !(loading || error) ? <View char={char}/> : null;

    return (
        <div className="random-block rounded">
            {errorMessage}
            {spinner}
            {content}
        </div>
    );
}

export default RandomChar

const View = ({char}) => {
    const {name,gender,born,died,culture} = char;
    return (
        <>
            <h4>Random Character: {name}</h4>
            <ul className="list-group list-group-flush">
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Gender </span>
                    <span>{gender}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Born </span>
                    <span>{born}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Died </span>
                    <span>{died}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Culture </span>
                    <span>{culture}</span>
                </li>
            </ul>
        </>
    )
}