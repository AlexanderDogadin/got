import React, {Component} from 'react';
import './randomChar.css';
import GotService from '../../services/gotService';
import Spinner from '../spiner';
import ErrorMessage from '../errorMessage';
import PropTypes from 'prop-types';

export default class RandomChar extends Component {

    gotService = new GotService();

    state = {
        char:{},
        loading: true,
        error: false
    }

    static defaultProps = {
        interval: 15000
    }

    static propTypes = {
        interval: PropTypes.number
    }

    onCharLoaded = (char) => {
        this.setState({
            char,
            loading:false
        });
    }

    onError = (err)=>{
        this.setState({
            // error:true,
            error: err,
            loading:false
        })
    }

    componentDidMount(){
        // console.log('mount')
        this.updateChar();
        this.timerID = setInterval(this.updateChar,this.props.interval);
    }
    componentDidUpdate() {
        // console.log('update')
    }
    componentWillUnmount  () {
        // console.log('unmount')
        clearInterval(this.timerID);
    }
    
    updateChar = () => {
        const id = Math.floor(Math.random()*140+25);
        // const id = 122938129838912839;
        this.gotService.getCharacter(id)
            .then(this.onCharLoaded)
            .catch(this.onError);
    }
   
    render() {
        // console.log('render')

        const {char, loading, error} = this.state;

        const errorMessage = error ? <ErrorMessage msg = {error.message}/> : null
        const spinner = loading ? <Spinner/> : null;
        const content = !(loading || error) ? <View char={char}/> : null;
 
        return (
            <div className="random-block rounded">
                {errorMessage}
                {spinner}
                {content}
            </div>
        );
    }
}

// Вынесено в класс как Static
// RandomChar.defaultProps = {
//     interval: 15000
// }

//Проверка на тип данных:
// RandomChar.propTypes = {
//     // interval: (props,propName,componentName)=>{
//     //     const value = props[propName];
//     //     if (typeof value === 'number' && ! isNaN(value)){
//     //         return null
//     //     }
//     //     return new TypeError(`${componentName}: ${propName} must be a number!`)
//     // }
//     // при использовании библиотеки проверки:
//     interval: PropTypes.number
// }

const View = ({char}) => {
    const {name,gender,born,died,culture} = char;
    return (
        <>
            <h4>Random Character: {name}</h4>
            <ul className="list-group list-group-flush">
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Gender </span>
                    <span>{gender}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Born </span>
                    <span>{born}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Died </span>
                    <span>{died}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between">
                    <span className="term">Culture </span>
                    <span>{culture}</span>
                </li>
            </ul>
        </>
    )
}