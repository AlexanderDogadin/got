import React from 'react';
import './errorMessage.css';
// import imgError from './error.png';
import imgError from './error.jpg';

const ErrorMessage = ({msg})=>{
    return (
        <>
            {/* из папки Public: */}
            {/* <img src = {process.env.PUBLIC_URL + '/img/error.jpg'} alt = 'error'></img> */}
            {/* <img src = {process.env.PUBLIC_URL + '/img/error.png'} alt = 'error'></img> */}
            {/* непосредственно с импортом картинки: */}
            <img src = {imgError} alt = 'error'></img>
            <span>Something goes wrong: </span> 
            <br/>
            <span>{msg}</span>
        </>
    )
    
}

export default ErrorMessage;