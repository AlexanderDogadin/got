import HousesPage from './housesPage';
import BooksPage from './booksPage';
import CharactersPage from './charactersPage';
import BooksItem from './booksItem';
import NoPage from './noPage';

export {
    CharactersPage,
    HousesPage,
    BooksPage,
    BooksItem,
    NoPage
} 