import React,{Component} from 'react';
import {Link} from 'react-router-dom'

export default class NoPage extends Component{
    render(){
        return(
            <div>
                <h1>Нет такой страницы</h1>
                <p>{this.props.url}</p>

                <Link to='/books/'>Вернуться на главную страницу</Link>
                <button>Вернуться на главную</button>
            </div>
        )
    }
}