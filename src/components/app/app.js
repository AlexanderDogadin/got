import React, { Component } from 'react';
import {Col, Row, Container} from 'reactstrap';
import Header from '../header';
import RandomChar from '../randomChar';
import './app.css';
import ErrorMessage from '../errorMessage'
import GotService from '../../services/gotService'
import {CharactersPage, HousesPage, BooksPage, BooksItem, NoPage} from '../pages'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'


export default class App  extends Component{
    
    gotService = new GotService();

    state = {
        showRandom: false,
        error: false
    }

    toggleRandom = () => {
        // this.setState((state)=>({
        //     showRandom: !state.showRandom
        // }))
        this.setState(({showRandom})=>({
            showRandom: !showRandom
        }))
    }

    componentDidCatch(){
        console.log(`error`);
        this.setState({error:true})
    }

    render () {

        if (this.state.error) {
            return <ErrorMessage/>
        }
        const {showRandom} = this.state;
        const showRandomChar = showRandom ? <RandomChar interval ='5000' /> : null
        // const showRandomChar = showRandom ? <RandomChar /> : null

        return (
            <Router>
                <div className = "app"> 
                    <Container>
                        <Header />
                    </Container>
                    <Container>
                        <Row>
                            <Col lg={{size: 5, offset: 0}}>
                                {/* <RandomChar/> */}
                                {showRandomChar}
                                <button
                                    onClick = {this.toggleRandom}
                                    className = 'toggle-btn'
                                    >
                                    Toggle random character
                                </button>
                            </Col>
                        </Row>
                        <Switch>
                            <Route path = '/characters' component = {CharactersPage}/>
                            <Route path = '/houses' component = {HousesPage}/>
                            <Route path = '/books' exact component = {BooksPage}/>
                            {/* динамический путь */}
                            <Route path = '/books/:id' 
                                render ={
                                    ({match}) => {
                                        const {id} = match.params;
                                        return <BooksItem bookId={id}/>}
                                } 
                            />
                            {/* <Route path = "*" component={NoPage}/> */}
                            <Route 
                                path = '*'
                                exact
                                render = {
                                    ({match}) =>{
                                        const {url} = match;
                                        console.log('match',match);
                                        return url!=='/' ? <NoPage url = {url}/> : null
                                    }
                                }
                            /> 
                        </Switch>
                    </Container>
                </div>
            </Router>
        );
    }
};
