import React from 'react';
import './spinner.css';

const Spinner = () => {
    return(
        <div className="loadingio-spinner-spinner-ryes9xye8f">
            <div className="ldio-8f86h1d74xi">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    )
}

export default Spinner;
