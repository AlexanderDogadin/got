import React, {Component} from 'react';
import './itemList.css';
import Spinner from '../spiner';
import ErrorMessage from '../errorMessage';
import PropTypes from 'prop-types';

export default class ItemList extends Component {
    state = {
        itemList: null,
        error: null
    }
    
    static defaultProps = {
        onItemSelected: ()=>{}
    }

    static propTypes = {
        onItemSelected: PropTypes.func,
        // getData:PropTypes.arrayOf(PropTypes.object)
    }

    componentDidMount(){
        const {getData} = this.props;

        getData()
            .then(
                (itemList) => {
                    this.setState({
                        itemList,
                        error: null
                    })
                })
            .catch((err)=>{
                console.log("err:",err);
                this.setState({error:err})
            })
    }

    renderItems(arr){
        return arr.map((item,i)=>{
            const {id} = item;
            const label = this.props.renderItem(item);

            return(
                <li 
                    key = {id}
                    className="list-group-item"
                    onClick = {()=>this.props.onItemSelected(item.id)}
                    >
                    {label}
                </li>
            )
        })

    }

    render() {
        const {itemList,error} = this.state;
        
        if (error){return <ErrorMessage msg = {error.message}/>}

        if (!itemList) {
            return (
                <Spinner/>
            )
        }

        const items = this.renderItems(itemList);

        return (
            <ul className="item-list list-group">
                {items}
            </ul>
        );
    }
}

// вынесено как static
// ItemList.defaultProps = {
//     onItemSelected: ()=>{}
// }

// ItemList.propTypes = {
//     onItemSelected: PropTypes.func,
//     getData:PropTypes.arrayOf(PropTypes.object)
// }
