import React, {useState, useEffect} from 'react';
import './itemList.css';
import Spinner from '../spiner';
import ErrorMessage from '../errorMessage';

function ItemList ({getData, renderItem, onItemSelected}) {

    const [state,updateState] = useState({itemList:null, error:false})

    function updateItemList(data) { 
        updateState(prevState =>({
            ...prevState,
            itemList:data,
            error:false
            })
        )
    }

    function setError(err) { 
        updateState(prevState =>({
            ...prevState,
            error:err
            })
        )
    }

    useEffect(()=>{
        getData()
        .then((data) => {
                updateItemList(data)
            })
        .catch((err)=>{
            setError(err)
        })

    },[])  

    function renderItems(arr){
        return arr.map((item)=>{
            const {id} = item;
            const label = renderItem(item);

            return(
                <li 
                    key = {id}
                    className="list-group-item"
                    onClick = {()=> onItemSelected(id)}
                    >
                    {label}
                </li>
            )
        })

    }

    if (state.error){return <ErrorMessage msg = {state.error.message}/>}

    if (!state.itemList) {
        return (
            <Spinner/>
        )
    }

    const items = renderItems(state.itemList);

    return (
        <ul className="item-list list-group">
            {items}
        </ul>
    );
}

export default ItemList